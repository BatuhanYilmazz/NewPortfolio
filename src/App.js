import React, { Suspense, useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import GlobalStyle from "./styles/GlobalStyle";
import Spinner from "./layout/Spinner/Spinnerr";
import { ThemeProvider } from "styled-components";
import NightMode from "./layout/NightMode/NightMode";
import Scroll from "./layout/Scroll/Scroll";

//React Lazy
const ProjectDetails = React.lazy(() =>
  import("./pages/ProjectDetails/ProjectDetails")
);
const Menu = React.lazy(() => import("./layout/Navbar/Menu"));
const Landing = React.lazy(() => import("./pages/Landing/Landing"));
const Projects = React.lazy(() => import("./pages/Projects/Projects"));
const Services = React.lazy(() => import("./pages/Services/Services"));
const About = React.lazy(() => import("./pages/About/About"));
const Footer = React.lazy(() => import("./layout/Footer/Footer"));

//
function App() {
  const [theme, setTheme] = useState({ mode: "light" });
  const handleClick = () => {
    setTheme(theme.mode === "dark" ? { mode: "light" } : { mode: "dark" });
  };

  return (
    <ThemeProvider theme={theme}>
      <Suspense fallback={<Spinner />}>
        <BrowserRouter>
          <div className='App'>
            <Menu />
            <Switch>
              <Route exact path='/' component={Landing} />
              <Route exact path='/projects' component={Projects} />
              <Route exact path='/projects/:id' component={ProjectDetails} />
              <Route exact path='/services' component={Services} />
              <Route exact path='/about' component={About} />
            </Switch>
            <GlobalStyle />
            <Footer />
            <Scroll />
            <NightMode handleClick={handleClick} />
          </div>
        </BrowserRouter>
      </Suspense>
    </ThemeProvider>
  );
}

export default App;
