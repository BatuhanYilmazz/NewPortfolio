import React from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faLinkedinIn,
  faGitlab,
  faGithub,
  faAngellist,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { Row, Container, Col, ListGroup } from "react-bootstrap";
import { motion } from "framer-motion";
import FooterStyled from "./FooterStyled";

const Footer = () => {
  return (
    <FooterStyled>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 1, ease: "easeInOut" }}
        key='footer'
      >
        <Container>
          <h1 className='text-white text-center mb-5 title'>
            Do you have an idea ? Let's <br />
            make it real together.
          </h1>

          <a
            target='_blank'
            href='mailto:a.batuhan.yilmaz@gmail.com'
            rel='noopener noreferrer'
            className='contact'
          >
            Let's Do It
          </a>

          <Row>
            <Col className='footer '>
              <ListGroup className='mt-5'>
                <ListGroup.Item>Batuhan Yılmaz</ListGroup.Item>
                <ListGroup.Item>
                  <NavLink to='/'>Home</NavLink>
                </ListGroup.Item>
                <ListGroup.Item>
                  <NavLink to='/projects'>Projects</NavLink>
                </ListGroup.Item>
                <ListGroup.Item>
                  <NavLink to='/services'>Services</NavLink>
                </ListGroup.Item>
                <ListGroup.Item>
                  <NavLink to='/about'>About</NavLink>
                </ListGroup.Item>
              </ListGroup>
              <ListGroup className='mt-5'>
                <ListGroup.Item>Lorem</ListGroup.Item>
                <ListGroup.Item>Lorem ipsum </ListGroup.Item>
                <ListGroup.Item>Lorem ipsum doler</ListGroup.Item>
              </ListGroup>
              <ListGroup className='mt-5'>
                <ListGroup.Item>Social</ListGroup.Item>
                <ListGroup.Item>
                  <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href='https://gitlab.com/BatuhanYilmazz'
                  >
                    <FontAwesomeIcon icon={faGitlab} size='lg' />
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href='https://github.com/BatuhanYilmazzz'
                  >
                    <FontAwesomeIcon icon={faGithub} size='lg' />
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href='https://angel.co/u/ahmet-batuhan-yilmaz'
                  >
                    <FontAwesomeIcon icon={faAngellist} size='lg' />
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href='https://www.linkedin.com/in/ahmetbatuhanyilmaz/'
                  >
                    <FontAwesomeIcon icon={faLinkedinIn} size='lg' />
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href='https://twitter.com/batuhnnylmazz'
                  >
                    <FontAwesomeIcon icon={faTwitter} size='lg' />
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href='mailto:a.batuhan.yilmaz@gmail.com'
                  >
                    <FontAwesomeIcon icon={faEnvelope} size='lg' />
                  </a>
                </ListGroup.Item>
              </ListGroup>
            </Col>
          </Row>
          <h6 className='copyright'>© Copyright 2020 Batuhan Yılmaz</h6>
          <h6 className='react'>Created with React</h6>
        </Container>
      </motion.div>
    </FooterStyled>
  );
};

export default Footer;
