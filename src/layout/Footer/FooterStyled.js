import styled from "styled-components";

const FooterStyled = styled.div`
  background-color: #000;
  padding: 7rem;
  text-align: center;

  h1 {
    font-size: 3.5rem;
    @media (max-width: 420px) {
      font-size: 2rem;
    }
  }
  @media (max-width: 420px) {
    padding: 5rem 3rem;
  }
  .footer {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    @media (max-width: 420px) {
      display: block;
      align-items: center;
    }
  }
  .title {
    font-weight: 400 !important;
  }

  .list-group {
    background-color: #000 !important;
    text-decoration: none !important;
  }
  .list-group-item,
  a {
    background-color: #000 !important;
    color: #7a7a7a !important;
    text-align: left !important;
    font-size: 18px;
    line-height: 25px;
    font-weight: 500;
    margin-bottom: 0.5rem;
    @media (max-width: 420px) {
      text-align: center !important;
    }
  }
  a {
    &:hover {
      color: #222 !important;
    }
  }

  .list-group-item:first-child {
    color: #fff !important;
  }

  .list-group-item:hover:not(:first-child) {
    color: #222 !important;
    cursor: pointer;
  }
  .copyright {
    color: #444;
    margin-top: 8rem;
    @media (max-width: 420px) {
      margin-top: 2rem;
    }
  }
  .react {
    color: #444;
    margin-top: 1rem;
  }
  .contact {
    display: inline-block;
    transition: all 0.3s ease-in;
    margin: 4rem auto;
    @media (max-width: 420px) {
      margin: 1rem auto;
    }
    background-color: #000 !important;
    color: #fff !important;
    border: 1px solid #f8f9fa !important;
    padding: 1rem 3rem;
    border-radius: 5px;
    &:hover {
      background-color: #fff !important;
      color: #000 !important;
    }
  }
`;

export default FooterStyled;
