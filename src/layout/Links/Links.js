import React from "react";
import { useContext } from "react";
import { ThemeContext } from "styled-components";
import { ListGroup } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faLinkedinIn,
  faGitlab,
  faGithub,
  faAngellist,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { motion } from "framer-motion";
import LinksStyled from "./LinksStyled";

const Links = () => {
  const themeContext = useContext(ThemeContext);
  const { mode } = themeContext;

  return (
    <LinksStyled mode={mode}>
      <motion.div
        initial={{ opacity: 0, y: -50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 1, delay: 1.7 }}
      >
        <ListGroup>
          <ListGroup.Item>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href='https://gitlab.com/BatuhanYilmazz'
            >
              <FontAwesomeIcon icon={faGitlab} size='lg' />
            </a>
          </ListGroup.Item>
          <ListGroup.Item>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href='https://github.com/BatuhanYilmazzz'
            >
              <FontAwesomeIcon icon={faGithub} size='lg' />
            </a>
          </ListGroup.Item>
          <ListGroup.Item>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href='https://angel.co/u/ahmet-batuhan-yilmaz'
            >
              <FontAwesomeIcon icon={faAngellist} size='lg' />
            </a>
          </ListGroup.Item>
          <ListGroup.Item>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href='https://www.linkedin.com/in/ahmetbatuhanyilmaz/'
            >
              <FontAwesomeIcon icon={faLinkedinIn} size='lg' />
            </a>
          </ListGroup.Item>
          <ListGroup.Item>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href='https://twitter.com/batuhnnylmazz'
            >
              <FontAwesomeIcon icon={faTwitter} size='lg' />
            </a>
          </ListGroup.Item>
          <ListGroup.Item>
            <a
              target='_blank'
              href='mailto:a.batuhan.yilmaz@gmail.com'
              rel='noopener noreferrer'
            >
              <FontAwesomeIcon icon={faEnvelope} size='lg' />
            </a>
          </ListGroup.Item>
        </ListGroup>
      </motion.div>
    </LinksStyled>
  );
};

export default Links;
