import styled from "styled-components";

const LinksStyled = styled.div`
  max-width: 180px;
  margin: 1rem auto;
  .list-group {
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-direction: row;
    list-style-type: none;
  }
  .list-group-item {
    margin-bottom: 1rem;
    border: none !important;
    border-radius: 0 !important;
    padding: 0 !important;
    background-color: transparent;
    a {
      color: ${(props) =>
        props.theme.mode === "dark" ? "#73737D" : "#000"} !important;
    }
  }
  svg {
    transition: all 0.3s ease-in;

    &:hover {
      transform: scale(1.2);
      color: #999 !important;
    }
  }
`;

export default LinksStyled;
