import React, { useState } from "react";
import { useContext } from "react";
import { ThemeContext } from "styled-components";
import { stack as Menu } from "react-burger-menu";
import { NavLink } from "react-router-dom";
import MobileMenuStyled from "./MobileMenuStyled";
const MobileMenu = () => {
  const themeContext = useContext(ThemeContext);
  const { mode } = themeContext;
  const [open, setOpen] = useState(false);
  const handleState = () => {
    setOpen(!open);
  };

  return (
    <MobileMenuStyled mode={mode}>
      <Menu isOpen={open} onStateChange={handleState} right>
        <NavLink to='/'>Home</NavLink>
        <NavLink to='/projects'>Projects</NavLink>
        <NavLink to='/services'>Services</NavLink>
        <NavLink to='/about'>About</NavLink>
      </Menu>
    </MobileMenuStyled>
  );
};

export default MobileMenu;
