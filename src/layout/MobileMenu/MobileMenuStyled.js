import styled from "styled-components";

const MobileMenuStyled = styled.div`
  button {
    border: none;
    background-color: #000;
    color: #fff;
  }
  a {
    color: #fff !important;
  }
  display: none;
  @media (max-width: 768px) {
    display: block;
    position: fixed;
    right: 0;
    top: 0;
    z-index: 99;
  }

  .bm-burger-button {
    position: fixed;
    width: 36px;
    height: 30px;
    right: 10px;
    top: 25px;
  }

  /* Color/shape of burger icon bars */
  .bm-burger-bars {
    background: ${(props) =>
      props.theme.mode === "dark" ? "#73737D" : "#000"} !important;
    height: 2px !important;
    width: 22px;
    &:nth-child(2) {
      top: 30% !important;
    }
    &:nth-child(3) {
      top: 60% !important;
    }
  }

  /* Color/shape of burger icon bars on hover*/
  .bm-burger-bars-hover {
    background: #000;
  }

  /* Position and sizing of clickable cross button */
  .bm-cross-button {
    height: 24px;
    width: 24px;
  }

  /* Color/shape of close button cross */
  .bm-cross {
    background: #fff !important;
    width: 2px !important;
    height: 20px !important;
  }

  /*
Sidebar wrapper styles
Note: Beware of modifying this element as it can break the animations - you should not need to touch it in most cases
*/
  .bm-menu-wrap {
    position: fixed;
    height: 100%;
  }

  /* General sidebar styles */
  .bm-menu {
    background: #000;
    font-size: 1.15em;
    margin: 0 auto;
    position: static !important;
  }

  /* Morph shape necessary with bubble or elastic */
  .bm-morph-shape {
    fill: #373a47;
  }

  /* Wrapper for item list */
  .bm-item-list {
    color: #b8b7ad;
    padding: 0.8em;

    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }

  /* Individual item */
  .bm-item {
    display: inline-block;
    font-size: 2.5rem;
    color: #fff !important;
    text-transform: uppercase;
    margin-bottom: 2rem;
  }

  /* Styling of overlay */
  .bm-overlay {
    background: rgba(0, 0, 0, 0.3);
  }
`;

export default MobileMenuStyled;
