import React from "react";
import { NavLink } from "react-router-dom";
import { Navbar, Container, Nav } from "react-bootstrap";
import { useContext } from "react";
import { ThemeContext } from "styled-components";
import MenuStyled from "./MenuStyled";
import MobileMenu from "../MobileMenu/MobileMenu";

const Menu = () => {
  const themeContext = useContext(ThemeContext);

  const { mode } = themeContext;
  return (
    <MenuStyled mode={mode}>
      <Navbar expand='lg'>
        <Container>
          <Navbar.Brand>
            <NavLink className='navbar-brand' to='/'>
              Batuhan Yılmaz
            </NavLink>
          </Navbar.Brand>

          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='ml-auto'>
              <NavLink className='nav-link' to='/projects'>
                Projects
              </NavLink>
              <NavLink className='nav-link' to='/services'>
                Services
              </NavLink>
              <NavLink className='nav-link' to='/about'>
                About
              </NavLink>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <MobileMenu />
    </MenuStyled>
  );
};

export default Menu;
