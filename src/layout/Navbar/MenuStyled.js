import styled from "styled-components";

const MenuStyled = styled.div`
  .hamburger-menu {
    position: absolute !important;
    right: 30px;
    top: 30px;
    z-index: 99999;
    span {
      background-color: black !important;
    }
  }
  .navbar-brand {
    color: ${(props) =>
      props.theme.mode === "dark" ? "#73737D" : "#000"} !important;
    font-size: 1.5rem !important;
    font-weight: 500;
    padding: 8px 0;
  }
  .navbar-toggler {
    border: none !important;

    &-icon {
      color: #000 !important;
    }
  }

  a:hover {
    color: #999 !important;
  }
  .nav-link {
    margin-left: 1.2rem;
    font-size: 1rem;
    color: ${(props) =>
      props.theme.mode === "dark" ? "#73737D" : "#000"} !important;
    font-weight: 500;
    padding: 1.2rem 1.5rem !important;
    letter-spacing: 0.1px;
    @media (max-width: 420px) {
      margin-left: 0;
      padding: 0;
    }
  }
  .nav-link:hover {
    color: #999 !important;
  }
`;

export default MenuStyled;
