import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import NightModeStyled from "./NightModeStyled";
const NightMode = ({ handleClick }) => {
  const [state, setState] = useState({
    moon: "block",
    sun: "none",
  });
  const handleState = () => {
    if (state.moon === "block") {
      setState({
        moon: "none",
        sun: "block",
      });
    }
    if (state.moon === "none") {
      setState({
        moon: "block",
        sun: "none",
      });
    }
  };

  return (
    <NightModeStyled>
      <button className='togglemoon' onClick={handleClick}>
        <FontAwesomeIcon
          icon={faMoon}
          size='lg'
          onClick={handleState}
          style={{ display: `${state.moon}` }}
        />
      </button>
      <button className='togglesun' onClick={handleClick}>
        <FontAwesomeIcon
          icon={faSun}
          size='lg'
          onClick={handleState}
          style={{ display: `${state.sun}` }}
        />
      </button>
    </NightModeStyled>
  );
};

export default NightMode;
