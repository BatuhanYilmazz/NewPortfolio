import styled from "styled-components";

const NightModeStyled = styled.div`
  position: fixed;
  right: 5rem;
  top: 2rem;
  @media (max-width: 420px) {
    right: 1rem;
    top: 95%;
  }

  .togglemoon {
    border: none;
    background-color: transparent;
    transition: all 1s ease-in;
    svg {
      transition: all 0.4s ease-in;
      color: #73737d;
      &:hover {
        color: #000;
        cursor: pointer;
      }
    }
  }
  .togglesun {
    border: none;
    background-color: transparent;
    transition: all 1s ease;

    svg {
      background-color: transparent;
      transition: all 0.4s ease-in;
      color: #73737d;
      &:hover {
        color: #fff;
        cursor: pointer;
      }
    }
  }
`;

export default NightModeStyled;
