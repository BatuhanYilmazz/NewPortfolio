import React from "react";
import { animateScroll as scroll } from "react-scroll";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowAltUp } from "@fortawesome/free-solid-svg-icons";
import ScrollStyled from "./ScrollStyled";

const Scroll = () => {
  const scrollToTop = () => {
    scroll.scrollToTop();
  };
  return (
    <ScrollStyled>
      <FontAwesomeIcon
        icon={faLongArrowAltUp}
        size='lg'
        onClick={scrollToTop}
      />
    </ScrollStyled>
  );
};

export default Scroll;
