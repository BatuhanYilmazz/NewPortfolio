import styled from "styled-components";

const ScrollStyled = styled.div`
  position: fixed;
  left: 4rem;
  bottom: 2rem;
  @media (max-width: 420px) {
    display: none;
  }

  svg {
    transition: all 0.3s ease-in;
  }
  svg:hover {
    transform: translateY(-10px);
    cursor: pointer;
  }
`;

export default ScrollStyled;
