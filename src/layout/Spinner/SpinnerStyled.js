import styled from "styled-components";

const SpinnerStyled = styled.div`
  min-height: 100vh;
  min-width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export default SpinnerStyled;
