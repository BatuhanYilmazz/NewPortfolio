import React from "react";
import { Spinner } from "react-bootstrap";
import SpinnerStyled from "./SpinnerStyled";
const Spinnerr = () => {
  return (
    <SpinnerStyled>
      <Spinner animation='grow' variant='dark' />
    </SpinnerStyled>
  );
};

export default Spinnerr;
