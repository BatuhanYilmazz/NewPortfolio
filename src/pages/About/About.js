import React from "react";
import { useContext } from "react";
import { ThemeContext } from "styled-components";
import { Row, Container, Col } from "react-bootstrap";
import { motion } from "framer-motion";
import AboutPageStyled from "../../styles/About/AboutPageStyled";
const About = () => {
  const themeContext = useContext(ThemeContext);
  const { mode } = themeContext;
  return (
    <AboutPageStyled mode={mode}>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        exit={{ opacity: 0, y: 50 }}
        transition={{ duration: 0.7 }}
        key='about'
      >
        <Container>
          <h1 className='head'>About</h1>
          <Row>
            <Col>
              <p>
                I don’t like to define myself by the work I’ve done. I define
                myself by the work I want to do. Skills can be taught,
                personality is inherent. I prefer to keep learning, continue
                challenging myself, and do interesting things that matter.
                <br /> <br />
                Fueled by high energy levels and boundless enthusiasm, I’m
                easily inspired and more then willing to follow my fascinations
                wherever they take me. I’m passionate, expressive,
                multi-talented spirit with a natural ability to entertain and
                inspire. I’m never satisfied to just come up with ideas. Instead
                I have an almost impulsive need to act on them. <br /> <br /> My
                abundant energy fuels me in the pursuit of many interests,
                hobbies, areas of study and artistic endeavors. I’m a fast
                learner, able to pick up new skills and juggle different
                projects and roles with relative ease. <br /> <br /> I’m a
                people-person with deep emotions and empathy. I’m able to
                inspire and am at my best when I’m sharing my creative
                expressions with others. I currently work as a Software
                Engineer. Outside of my working I work with a select freelance
                client base.
              </p>
              <p>
                If we mention about education, I graduated with a degree in
                Aeronautical Engineering from the Turkish Air Force Academy ,
                had worked for several years as a project officer at Turkish Air
                Force and received 8+ certificates of excellence from the top
                commanders of the Turkish Air Force before making the switch to
                software dev. Design has been always my passion. I decided to
                give a chance to myself. If you'd like to talk,feel free to
                reach out!
              </p>
            </Col>
          </Row>
        </Container>
      </motion.div>
    </AboutPageStyled>
  );
};

export default About;
