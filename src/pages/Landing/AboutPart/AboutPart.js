import React from "react";
import { Row, Container, Col } from "react-bootstrap";
import AboutPartStyled from "../../../styles/Landing/AboutPartStyled";
const AboutPart = () => {
  return (
    <AboutPartStyled>
      <Container>
        <Row>
          <Col sm={12} md={5}>
            <h1 className='display-4 font-weight-bold mb-4'>About Me</h1>
            <h2>
              I am Batuhan Yılmaz, Software Engineer from Turkey based in
              Istanbul. <br />
              <br /> I design and code beautifully simple things, and I love
              what I do.
            </h2>
            <br />
          </Col>
          <Col sm={12} md={7}>
            <h3>What includes my skill ? </h3>
            <p className='skills'>
              Html5,Css3,Bootstrap,Javascript,Reactjs,React-Bootstrap,Nextjs,Nodejs,Expressjs,MongoDB
            </p>
            <br />
            <br />
            <h3>What is the content of my work?</h3>
            <p>
              I mainly create new websites with Reactjs or Nextjs. From the
              conception through the design to the implementation, I take care
              of most of the tasks myself and advise customers in every aspect
              of their online presence. I offer you an ideal combination of
              creativity and technical understanding. My design is simple,
              elegant and focused on the essentials. Adapted to your target
              group and usable for everyone. I will deliver a customized
              solution.
            </p>
            <br />
            <br />

            <h3>What are my values?</h3>
            <p>
              Problem Solving <br /> I can take vague problems and requirements
              and break them down into steps and solutions. <br /> <br />{" "}
              Systems Thinking <br /> I'm good at thinking abstractly and
              putting together systems with many moving parts. <br /> <br />{" "}
              Communicating <br /> I can explain things clearly, communicate
              problems quickly and write accurately and concisely. <br /> <br />{" "}
              Organising <br /> I can self-manage, work to deadlines, organise
              projects and present well-structured and complete deliverables.
            </p>
            <br />
            <br />
            <h3>What I like about working as a web designer?</h3>
            <p>
              Behind every project is a new challenge, a new client, a new goal,
              a new solution. So are you looking for a professional,
              communicative & punctual software engineer with extensive web
              development skills? If you have an website you are interested in
              developing, a problem that needs solving or a project that needs
              rescuing, I'd love to help you with it.
            </p>
            <br />
            <br />
          </Col>
        </Row>
      </Container>
    </AboutPartStyled>
  );
};

export default AboutPart;
