import React from "react";
import { Row, Col } from "react-bootstrap";
import { motion } from "framer-motion";
import HeaderPartStyled from "../../../styles/Landing/HeaderPartStyled";
import Links from "../../../layout/Links/Links";
const Header = () => {
  return (
    <HeaderPartStyled>
      <Row>
        <Col sm={12} className='text-center'>
          <motion.div
            className='wrapper'
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ duration: 1 }}
          >
            <motion.img
              initial={{ opacity: 0, y: 100 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ duration: 1.5, delay: 1 }}
              src='./images/myphoto.jpg'
              alt='profil'
              className='profileimage'
            />
            <motion.h1
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ duration: 2 }}
              className='title'
            >
              Frontend Developer <br /> (React & React Native)
            </motion.h1>
            <motion.p
              className='content'
              initial={{ opacity: 0, y: -50 }}
              animate={{ opacity: 1.5, y: 0 }}
              transition={{ duration: 1.5, delay: 1 }}
            >
              Everything you can image is real. <br />
              Let's make it real.
            </motion.p>
            <Links />
          </motion.div>
        </Col>
      </Row>
    </HeaderPartStyled>
  );
};

export default Header;
