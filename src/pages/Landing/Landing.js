import React, { Suspense } from "react";
import { Container } from "react-bootstrap";
import { motion } from "framer-motion";

import TestimonialsPart from "./Testimonials/TestimonialsPart";
import Spinnerr from "../../layout/Spinner/Spinnerr";

const Header = React.lazy(() => import("./Header/Header"));
const ServicePart = React.lazy(() => import("./ServicePart/ServicePart"));
const MyServe = React.lazy(() => import("./MyServe/MyServe"));
const AboutPart = React.lazy(() => import("./AboutPart/AboutPart"));

const Landing = () => {
  return (
    <motion.div
      initial={{ opacity: 0, y: 0 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.7 }}
    >
      <Container>
        <Suspense fallback={<Spinnerr />}>
          <Header />
          <TestimonialsPart />
          <ServicePart />
          <MyServe />
          <AboutPart />
        </Suspense>
      </Container>
    </motion.div>
  );
};

export default Landing;
