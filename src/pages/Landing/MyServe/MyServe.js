import React from "react";
import { Row, Container, Col } from "react-bootstrap";

import MyServePartStyled from "../../../styles/Landing/MyServePartStyled";
const MyServe = () => {
  return (
    <MyServePartStyled>
      <Container>
        <Row>
          <Col sm={12} md={5}>
            <h1 className='display-4 font-weight-bold mb-4'>My Serve</h1>

            <h2>
              I like to code things from scratch, and enjoy making ideas real by
              using newest technology.
            </h2>
            <br />
          </Col>
          <Col sm={12} md={7}>
            <h3>UI/UX Design</h3>
            <p>
              UI/UX involves planning and iterating a site's structure and
              layout. Once the proper information architecture is in place, I
              design the visual layer to create a beautiful user experience.
            </p>
            <br />
            <br />
            <h3>Web Design</h3>
            <p>
              Simplicity is one of the golden rules of website design. The
              audience should have an enjoyable, positive experience when using
              your website. Whether their objective is reading content, watching
              a video or enrolling in a course, every action should be clear and
              concise throughout the website. Our approach is to create a
              website that strengthens your company’s brand while ensuring ease
              of use and simplicity for your audience.
            </p>
            <br />
            <br />

            <h3>Development</h3>
            <p>
              The website design process starts with a pen and paper to sketch
              page layouts, wire-frames, sitemaps and menu structures. Digital
              design concepts are then created incorporating your company’s
              brand guidelines for a personalised look and feel. You will be
              presented with a variety of website design concepts to review.
              Feedback on the design concepts is essential to give you the
              opportunity to express your thoughts on the design and make
              alterations where desired before the final sign-off.
            </p>
            <br />
            <br />
            <h3>CMS & E-Commerce</h3>
            <p>
              Fully custom databases for dynamic content types, including online
              stores. Create the content structure you want, add content
              manually, or import it from a CSV file, and then design it
              visually. Finally, a content management system that works for
              clients and designers.
            </p>
            <br />
            <br />
            <h3>Animations</h3>
            <p>
              With CSS and Reactjs animation libraries ,I can create create
              complex rich animationed UI with full power of CSS and JavaScript
              .
            </p>
            <br />
            <br />
          </Col>
        </Row>
      </Container>
    </MyServePartStyled>
  );
};

export default MyServe;
