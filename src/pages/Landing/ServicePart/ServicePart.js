import React from "react";
import { Row, Container, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDesktop,
  faDatabase,
  faDiceD6,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import ServicePartStyled from "../../../styles/Landing/ServicePartStyled";

const ServicePart = () => {
  return (
    <ServicePartStyled>
      <Container>
        <Row>
          <Col sm={12} md={5}>
            <h1 className='display-4 font-weight-bold'>Service</h1>
            <h2>
              I value simple content structure, clean design patterns, and
              thoughtful interactions.
            </h2>
            <br />
          </Col>
          <Col sm={12} md={4}>
            <FontAwesomeIcon icon={faDesktop} size='3x' className='my-4' />
            <h3>Responsive Design</h3>
            <p>
              Websites I created are designed according to all screen sizes and
              mobile devices.
            </p>
            <br />
            <br />
            <FontAwesomeIcon icon={faSearch} size='3x' className='my-4' />
            <h3> Optimized for search engines</h3>
            <p>
              I optimize your website for search engines like Google and Yahoo,
              because a successful online appearance should be found.
            </p>
          </Col>
          <Col sm={12} md={3}>
            <FontAwesomeIcon icon={faDatabase} size='3x' className='my-4' />
            <h3>CMS</h3>
            <p>
              A content management system allows you to maintain and update your
              website yourself. Once we build it, you can use forever without no
              doubt.
            </p>
            <FontAwesomeIcon icon={faDiceD6} size='3x' className='my-4' />
            <h3> Content Strategy & Usability</h3>
            <p>
              The right content structure and intuitiveness improve the user
              experience.
            </p>
          </Col>
        </Row>
      </Container>
    </ServicePartStyled>
  );
};

export default ServicePart;
