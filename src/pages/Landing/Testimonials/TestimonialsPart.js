import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import TestimonialsPartStyled from "../../../styles/Landing/TestimonialsPartStyled";
const TestimonialsPart = () => {
  const settings = {
    dots: true,
    className: "center",
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    speed: 800,
    autoplaySpeed: 5000,
    cssEase: "linear",
  };
  return (
    <TestimonialsPartStyled>
      <Slider {...settings}>
        <div>
          <h1>"Just give him a pc and see the magic."</h1>
          <h6>Yunus Emre Aybey</h6>
          <p>Team Lead - Getir</p>
        </div>
        <div>
          <h1>"He is not great. He is perfect"</h1>
          <h6>Bora Gündüz</h6>
          <p>UI/UX Designer - Karbonat</p>
        </div>
        <div>
          <h1>"The most amazing developer I have ever worked with."</h1>
          <h6>Alper Tütüncü</h6>
          <p>UI/UX Designer - Mars Investment</p>
        </div>
      </Slider>
    </TestimonialsPartStyled>
  );
};

export default TestimonialsPart;
