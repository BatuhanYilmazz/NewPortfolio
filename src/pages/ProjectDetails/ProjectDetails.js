import React from "react";
import { useContext } from "react";
import { ThemeContext } from "styled-components";
import { useParams } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { Row, Container, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLongArrowAltLeft,
  faLongArrowAltRight,
} from "@fortawesome/free-solid-svg-icons";
import { motion } from "framer-motion";
import { projects } from "../../assets/content.json";

import ProjectDetailsPageStyled from "../../styles/ProjectDetails/ProjectDetailsPageStyled";
const ProjectDetails = () => {
  const themeContext = useContext(ThemeContext);
  const { mode } = themeContext;
  let { id } = useParams();
  const project = projects.find((x) => x.id === id);

  return (
    <ProjectDetailsPageStyled mode={mode}>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        exit={{ opacity: 0, y: 50 }}
        transition={{ duration: 0.7 }}
        key='projectdetails'
      >
        <Container>
          <div className='back'>
            <FontAwesomeIcon icon={faLongArrowAltLeft} size='lg' />
            <NavLink to='/projects' className='ml-2'>
              Back
            </NavLink>
          </div>
          <Row>
            <Col className='text-center'>
              <div className='wrapper'>
                <div className='full-website-image-container'>
                  <div className='full-website-image-wrapper'>
                    <img
                      src={project.macbook}
                      alt=''
                      className='website-image '
                    />
                  </div>
                </div>
                <img
                  src='/images/macbook.png'
                  alt='macbook'
                  className='macbook'
                />
              </div>
            </Col>
          </Row>
          <div className='flex-col-wrapper'>
            <div className='flex-col'>
              <p>Project</p>
              <h4>{project.title}</h4>
            </div>
            <div className='flex-col'>
              <p>Online Since</p>
              <h4>November 2019</h4>
            </div>
            <div className='flex-col live'>
              <a
                href={project.websitelive}
                target='_blank'
                className='mr-2'
                without
                rel='noopener noreferrer'
              >
                Visit live website
              </a>
              <FontAwesomeIcon icon={faLongArrowAltRight} size='lg' />
            </div>
          </div>
          <div className='comment'>
            <h4>Landing page for {project.title}</h4>
          </div>
        </Container>
      </motion.div>
    </ProjectDetailsPageStyled>
  );
};

export default ProjectDetails;
