import React from "react";
import { useContext } from "react";
import { ThemeContext } from "styled-components";
import { NavLink } from "react-router-dom";
import { Row, Container, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowAltRight } from "@fortawesome/free-solid-svg-icons";
import ProjectsPageStyled from "../../styles/Projects/ProjectsPageStyled";
import { motion } from "framer-motion";
import { projects } from "../../assets/content.json";

const Projects = () => {
  const themeContext = useContext(ThemeContext);
  const { mode } = themeContext;
  return (
    <ProjectsPageStyled mode={mode}>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 1 }}
      >
        <Container>
          <h1 className='head'>Projects</h1>
          {projects.map((project) => (
            <Row key={project.id}>
              <Col
                sm={12}
                md={6}
                className='d-flex justify-content-between align-content-between flex-column'
              >
                <div>
                  <h1 className='title'> {project.title}</h1>
                  <motion.p
                    initial={{ opacity: 0, y: -50 }}
                    animate={{ opacity: 1, y: 0 }}
                    transition={{ duration: 1, delay: 0.1 }}
                  >
                    {project.usedstacks}
                  </motion.p>
                </div>
              </Col>
              <Col sm={12} md={6} className='p-4'>
                <motion.img
                  initial={{ opacity: 0, x: -250 }}
                  animate={{ opacity: 1, x: 0 }}
                  transition={{ duration: 1, delay: 0.1 }}
                  className='img'
                  src={project.showcase}
                  alt='1'
                />
              </Col>
              <Col sm={12}>
                <div className='toproject'>
                  <NavLink to={`projects/${project.id}`}>
                    To The Project
                  </NavLink>
                  <FontAwesomeIcon
                    icon={faLongArrowAltRight}
                    size='lg'
                    className='ml-3'
                  />
                </div>
              </Col>
            </Row>
          ))}
        </Container>
      </motion.div>
    </ProjectsPageStyled>
  );
};

export default Projects;
