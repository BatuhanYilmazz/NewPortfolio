import React from "react";
import { useContext } from "react";
import { ThemeContext } from "styled-components";
import { motion } from "framer-motion";
import { Row, Container, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDesktop,
  faDatabase,
  faDiceD6,
  faSearch,
  faFire,
  faLock,
} from "@fortawesome/free-solid-svg-icons";
import ServicesPageStyled from "../../styles/Services/ServicesPageStyled";
const Services = () => {
  const themeContext = useContext(ThemeContext);
  const { mode } = themeContext;
  return (
    <ServicesPageStyled mode={mode}>
      <motion.div
        initial={{ opacity: 0, y: 50 }}
        animate={{ opacity: 1, y: 0 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.7 }}
        key='services'
      >
        <Container>
          <h1 className='head'>Services</h1>
          <Row>
            <Col sm={12} md={4}>
              <FontAwesomeIcon icon={faDesktop} size='2x' className='my-4' />
              <h2>Responsive Design</h2>
              <p>
                Websites that work on all devices and screen resolutions, so
                your visitors have the best user experience. With Responsive
                Design, too little text on smartphones and back and forth
                zooming are a thing of the past.
              </p>
            </Col>
            <Col sm={12} md={4}>
              <FontAwesomeIcon icon={faDatabase} size='2x' className='my-4' />
              <h2> Content Management System</h2>
              <p>
                Complete and efficient control of your website. In simple terms,
                a CMS is a database that manages the contents (images, texts,
                etc.). By linking this database to elements on the website, they
                become dynamic. Thus, a blog with hundreds of entries can be
                easily implemented and managed.
              </p>
            </Col>
            <Col sm={12} md={4}>
              <FontAwesomeIcon icon={faSearch} size='2x' className='my-4' />
              <h2> SEO</h2>
              <p>
                Search engine optimization is very important, because your
                website deserves to be found in search engines like Google, Bing
                and Yahoo. Thats why I already have the search engine
                optimization in mind during the design process. This increases
                your visitor numbers and ideally your sales.
              </p>
            </Col>
          </Row>
          <Row>
            <Col sm={12} md={4}>
              <FontAwesomeIcon icon={faLock} size='2x' className='my-4' />
              <h2>SSL-Encryption</h2>
              <p>
                Security is an important issue these days. It's good to know
                that my websites are encrypted using SSL. Data is exchanged
                between the server and the browser of your visitors via a secure
                connection. Without encryption, all data to be transmitted can
                be viewed on the Internet and manipulated by third parties with
                little effort.
              </p>
            </Col>
            <Col sm={12} md={4}>
              <FontAwesomeIcon icon={faFire} size='2x' className='my-4' />
              <h2> Fast Loading Times</h2>
              <p>
                Nothing disturbs surfing the web more than a website that loads
                very slowly. Already 40% of the visits leave the site if the
                loading time is more than 3 seconds. Google also measures the
                loading time of a website and ranked the page accordingly in the
                search results. The speed optimization included in my service
                ensures more visitors and an increased chance to rank 1st place
                on Google.
              </p>
            </Col>
            <Col sm={12} md={4}>
              <FontAwesomeIcon icon={faDiceD6} size='2x' className='my-4' />
              <h2> Content Strategy & Usability</h2>
              <p>
                The right content structure and intuitive user guidance improve
                the user experience. Therefore, it is important to define the
                content first so that the design can be adapted to the content &
                message as best as possible later on. Not the other way around.
              </p>
            </Col>
          </Row>
        </Container>
      </motion.div>
    </ServicesPageStyled>
  );
};

export default Services;
