import styled from "styled-components";

const AboutPageStyled = styled.div`
  padding: 0 4rem;
  margin: 5rem auto;
  color: ${(props) =>
    props.theme.mode === "dark" ? "#73737D" : "#000"} !important;
  min-height: 70vh;
  @media (max-width: 420px) {
    padding: 0 1rem;
    margin: 2rem auto;
  }
  .container {
    @media (max-width: 420px) {
      padding: 0 !important;
    }
  }

  .col-md-4 {
    padding: 0 2rem;
    @media (max-width: 420px) {
      padding: 0 0.5rem;
    }
  }
  .row {
    padding: 0 1rem;
    margin-bottom: 2rem;
    @media (max-width: 420px) {
      padding: 0;
    }
  }

  .head {
    font-weight: 600;
    font-size: 3rem;
    margin-top: 2rem;
    margin-bottom: 4rem;
    @media (max-width: 420px) {
      margin-top: 1rem;
      margin-bottom: 2rem;
    }
  }

  h2 {
    font-size: 1.7rem;
  }
  p {
    font-size: 1.5rem;
  }
`;

export default AboutPageStyled;
