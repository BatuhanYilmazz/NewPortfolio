import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
@font-face {
    font-family: 'Titillium Web';
    font-weight: 700;
    src: url(./fonts/TitilliumWeb-Bold.ttf);
}
@font-face {
    font-family: 'Titillium Web';
    font-weight: 400;
    src:  url(./fonts/TitilliumWeb-Regular.ttf);
}
@font-face {
    font-family: 'Titillium Web';
    font-weight: 600;
    src:  url(./fonts/TitilliumWeb-SemiBold.ttf);
}
 * {
  margin: 0;
  padding: 0;
}
html{
  overflow-x:hidden;
  @media (max-width: 768px) {
     font-size:75%;
    }
}
body {
    font-family: 'Titillium Web', sans-serif;
    overflow-x:hidden;
    background-color:${(props) =>
      props.theme.mode === "dark" ? "#111216" : "#fff"};
    color:${(props) => (props.theme.mode === "dark" ? "#000" : "#000")};
    transition:all .4s ease-in;
}
.container {
  max-width: 1350px !important;
  margin: auto;
}
a {
  color: #000 !important;
}
a:hover {
  text-decoration: none !important;
}

`;

export default GlobalStyle;
