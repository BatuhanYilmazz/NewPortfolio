import styled from "styled-components";

const AboutPartStyled = styled.div`
  background-image: linear-gradient(0deg, #fafafa, rgba(250, 224, 224, 0.63));
  /*  #fae0e1*/
  padding: 5rem 4rem;
  margin: 3rem auto 10rem auto;
  @media (max-width: 768px) {
    padding: 2.5rem 1.5rem;
  }
  h1 {
    @media (max-width: 768px) {
      font-size: 2.5rem;
    }
  }
  h2 {
    @media (max-width: 768px) {
      font-size: 1.5rem;
    }
  }
  h3 {
    font-weight: 600;
  }
  .skills {
    font-size: 1.5rem;
    @media (max-width: 768px) {
      font-size: 1.2rem;
    }
  }
`;

export default AboutPartStyled;
