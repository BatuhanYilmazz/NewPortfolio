import styled from "styled-components";

const HeaderPartStyled = styled.div`
  .row {
    min-height: 70vh;
  }
  margin-top: 4rem;
  margin-bottom: 2rem;
  @media (max-width: 768px) {
    margin-bottom: 1rem;
    margin-top: 3rem;
  }

  svg {
    &:hover {
      color: #777;
      cursor: pointer;
    }
  }
  .profileimage {
    max-width: 12rem;
    margin: 1rem auto 0 auto;
    border-radius: 100%;
    transition: border 0.5s ease-in;
    border: 3px solid
      ${(props) => (props.theme.mode === "dark" ? "#000" : "#eee")};
  }
  .wrapper {
    position: relative;
    padding-top: 4rem;
    @media (max-width: 768px) {
      padding-top: 0.5rem;
    }
  }

  .title {
    color: ${(props) => (props.theme.mode === "dark" ? "#73737D" : "#222")};
    font-size: 3rem;
    margin: 2rem auto;
    font-weight: 600;
    max-width: 50rem;
  }

  .content {
    color: ${(props) => (props.theme.mode === "dark" ? "#73737D" : "#222")};
    font-size: 1.2rem;
    font-weight: 500;
    max-width: 40rem;
    margin: 2rem auto;
  }
`;

export default HeaderPartStyled;
