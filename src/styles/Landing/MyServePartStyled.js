import styled from "styled-components";

const MyServePartStyled = styled.div`
  background-image: linear-gradient(180deg, #e0f3fa, rgba(224, 238, 250, 0.63));
  padding: 5rem 4rem;
  margin: 3rem auto;
  @media (max-width: 768px) {
    padding: 2.5rem 1.5rem;
  }
  h1 {
    @media (max-width: 768px) {
      font-size: 2.5rem;
    }
  }
  h2 {
    @media (max-width: 768px) {
      font-size: 1.5rem;
    }
  }
  h3 {
    font-weight: 600;
  }
`;

export default MyServePartStyled;
