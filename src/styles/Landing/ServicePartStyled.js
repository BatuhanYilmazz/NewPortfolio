import styled from "styled-components";

const ServicePartStyled = styled.div`
  padding: 5rem 4rem 10rem 4rem;
  margin: 3rem auto;
  @media (max-width: 768px) {
    padding: 2.5rem 1.5rem;
  }
  background-image: linear-gradient(180deg, #fff2d7, rgba(255, 238, 230, 0.63));
  h1 {
    @media (max-width: 768px) {
      font-size: 2.5rem;
    }
  }
  h2 {
    @media (max-width: 768px) {
      font-size: 1.5rem;
    }
  }
  h3 {
    font-weight: 600;
  }
`;

export default ServicePartStyled;
