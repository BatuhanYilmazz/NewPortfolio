import styled from "styled-components";

const TestimonialsPartStyled = styled.div`
  text-align: center;
  padding: 3rem 0.5rem;
  background-color: #fff5f5;
  @media (max-width: 420px) {
    padding: 2.5rem 0.5rem;
  }
  h1 {
    font-weight: 400;
    @media (max-width: 420px) {
      font-size: 2rem;
    }
  }

  h6 {
    font-size: 1.3rem;
    margin-top: 1rem;
    @media (max-width: 420px) {
      font-weight: 400;
    }
  }

  p {
    margin-top: 1rem;
    font-size: 1rem;
  }
`;

export default TestimonialsPartStyled;
