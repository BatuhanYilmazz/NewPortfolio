import styled from "styled-components";

const ProjectDetailsPageStyled = styled.div`
  margin-top: 2rem;
  color: ${(props) =>
    props.theme.mode === "dark" ? "#73737D" : "#000"} !important;
  @media (max-width: 768px) {
    margin-top: 6rem;
    z-index: 5;
    position: relative;
  }
  .flex-col-wrapper {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin: 1rem 2rem;
    border-bottom: 2px solid #eee;
    text-align: center;
    padding: 1rem 10rem 2rem 10rem;
    @media (max-width: 420px) {
      display: block;
      padding: 1rem 3rem 2rem 3rem;
    }
    .flex-col {
      @media (max-width: 420px) {
        margin-bottom: 3rem;
      }

      p {
        @media (max-width: 420px) {
          margin-bottom: 0.2rem;
        }
      }
    }

    .live {
      svg {
        transition: all 0.3s ease-in;
      }
      &:hover {
        svg {
          transform: translateX(10px);
        }
      }
    }
  }
  p {
    color: #929292;
  }
  svg {
    transition: all 0.3s ease-in;
  }
  .back {
    &:hover {
      svg {
        transform: translateX(-10px) !important;
        cursor: pointer;
      }
    }
  }
  .wrapper {
    max-width: 58rem;
    margin: 3rem auto 1rem auto;
    position: relative;
  }

  .full-website-image-container {
    position: absolute;
    left: 0;
    top: 6%;
    right: 0;
    bottom: 0;
    display: flex;
    justify-content: center;
    align-items: flex-start;
  }
  .full-website-image-wrapper {
    position: static;
    left: auto;
    top: 6%;
    right: auto;
    bottom: auto;
    display: block;
    overflow: scroll;
    z-index: 10;
    width: 76%;
    height: 80.5%;
    margin-left: 0;
  }
  .website-image {
    position: relative;
    left: auto;
    top: 0;
    right: auto;
    bottom: auto;
    z-index: 0;
    width: 100%;
    max-width: none;
    margin-right: 0;
    margin-left: 0;
  }

  .macbook {
    max-width: 100%;
  }
  .comment {
    margin: 4rem auto;
    text-align: center;
  }
  a {
    color: ${(props) =>
      props.theme.mode === "dark" ? "#73737D" : "#000"} !important;
  }
`;

export default ProjectDetailsPageStyled;
