import styled from "styled-components";

const ProjectsPageStyled = styled.div`
  padding: 0 4rem;
  margin: 5rem auto 10rem auto;
  color: #000 !important;
  .container {
    @media (max-width: 420px) {
      padding: 0 !important;
    }
  }
  @media (max-width: 420px) {
    padding: 0 1rem;
  }

  .head {
    font-weight: 600;
    font-size: 3rem;
    margin-top: 2rem;
  }

  .row {
    background-color: ${(props) =>
      props.theme.mode === "dark" ? "#999" : "#f8f8f8"} !important;
    padding: 2rem 1.5rem !important;
    margin: 2rem auto !important;
    @media (max-width: 768px) {
      padding: 2rem 0 !important;
    }
  }
  p {
    padding-right: 3rem;
  }
  .title {
    font-size: 2.5rem;
    font-weight: 600;
    margin-bottom: 1.5rem;
    @media (max-width: 768px) {
      font-size: 2rem;
    }
  }
  .img {
    max-width: 100%;
    box-shadow: -1px 0px 6px -1px rgba(0, 0, 0, 0.75);
  }
  svg {
    transition: all 0.2s ease-in;
  }

  .toproject {
    &:hover {
      svg {
        transform: translateX(10px) !important;
        cursor: pointer;
      }
    }
  }
`;

export default ProjectsPageStyled;
